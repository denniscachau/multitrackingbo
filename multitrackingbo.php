<?php
/**
 * 2007-2023 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2023 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once _PS_MODULE_DIR_ . 'multitrackingbo/classes/MTB.php';

class Multitrackingbo extends Module
{
    protected $config_form = true;

    public function __construct()
    {
        $this->name = 'multitrackingbo';
        $this->tab = 'shipping_logistics';
        $this->version = '1.5.0';
        $this->author = 'Ciren';
        $this->need_instance = 1;
        $this->module_key = '3b7dbb2b54649741eadc91fcce6c6bc8';

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Multi carriers / tracking numbers');
        $this->description = $this->l('Multi carriers / tracking numbers');

        $this->ps_versions_compliancy = array('min' => '1.6.0.0', 'max' => '8.0.99');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (!parent::install()) {
            return false;
        }

        include(dirname(__FILE__).'/sql/install.php');

        Configuration::updateValue('MTB_display_invoice', 1);
        Configuration::updateValue('MTB_display_order_details', 1);
        Configuration::updateValue('MTB_display_mail', 1);
        Configuration::updateValue('MTB_show_all_carriers', 0);
        Configuration::updateValue('MTB_show_all_products', 0);
        Configuration::updateValue('MTB_NO_PRODUCTS', 0);
        Configuration::updateValue('MTB_SEND_EMAIL', 1);

        if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            Configuration::updateValue('PS_ORDER_RECALCULATE_SHIPPING', 0);
        }

        if (version_compare(_PS_VERSION_, '1.7.7.0', '<')) {
            $this->registerHook('displayAdminOrderTabShip');
            $this->registerHook('displayAdminOrderContentShip');
        } else {
            $this->registerHook('displayAdminOrderTabLink');
            $this->registerHook('displayAdminOrderTabContent');
        }

        if (version_compare(_PS_VERSION_, '1.7.5.0', '>=')) {
            $this->registerHook('actionOrderDeliverySlipPdfForm');
            $this->registerHook('actionOrderDeliverySlipPdfSave');
        } else {
            $this->registerHook('actionAdminDeliverySlipFormModifier');
        }

        return $this->addTab() &&
            $this->registerHook('displayBackOfficeHeader') &&
            $this->registerHook('displayOrderDetail') &&
            $this->registerHook('displayPDFInvoice') &&
            $this->registerHook('actionOrderStatusPostUpdate');
    }

    public function addTab()
    {
        if (!Tab::getIdFromClassName('AdminMultiTrackingBo')) {
            $tab = new Tab();
            $tab->class_name = 'AdminMultiTrackingBo';
            $tab->id_parent = (int)Tab::getIdFromClassName('AdminParentShipping');
            $tab->module = $this->name;
            $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->displayName;
            $tab->active = 0;
            $tab->add();
        }

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }

        Configuration::deleteByName('MTB_display_invoice');
        Configuration::deleteByName('MTB_display_order_details');
        Configuration::deleteByName('MTB_display_mail');
        Configuration::deleteByName('MTB_show_all_carriers');
        Configuration::deleteByName('MTB_show_all_products');
        Configuration::deleteByName('MTB_NO_PRODUCTS');
        Configuration::deleteByName('MTB_SEND_EMAIL');

        if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            Configuration::deleteByName('PS_ORDER_RECALCULATE_SHIPPING');
        }

        return $this->removeTab();
    }

    public function removeTab()
    {
        $id_tab = Tab::getIdFromClassName('AdminMultiTrackingBo');

        if ($id_tab) {
            $tab = new Tab($id_tab);
            $tab->delete();
        }

        return true;
    }

    public function getContent()
    {
        if (Tools::isSubmit('submitMTB')) {
            $this->postProcess();

            $link = new Link();
            if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $link = $link->getAdminLink(
                    'AdminModules',
                    true,
                    [],
                    [ 'configure' => $this->name, 'conf' => '4' ]
                );
            } else {
                $link = $link->getAdminLink('AdminModules', true) . '&configure=' . $this->name . '&conf=4';
            }
            Tools::redirectAdmin($link);
        }

        $this->context->smarty->assign(array(
            'mtb_doc_link' => _MODULE_DIR_ . 'multitrackingbo/readme_' . $this->getDocSuffix() . '.pdf'
        ));

        $doc = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/doc.tpl');

        return $doc . $this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitMTB';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    protected function getConfigFormValues()
    {
        return Configuration::getMultiple(array(
            'MTB_display_invoice',
            'MTB_display_order_details',
            'MTB_display_mail',
            'MTB_show_all_carriers',
            'MTB_show_all_products',
            'PS_ORDER_RECALCULATE_SHIPPING',
            'MTB_NO_PRODUCTS',
            'MTB_SEND_EMAIL'
        ));
    }

    private function getDocSuffix()
    {
        $available_docs = array(
            'fr'
        );

        return in_array($this->context->language->iso_code, $available_docs) ?
            $this->context->language->iso_code :
            'en';
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        $form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'name' => 'MTB_display_invoice',
                        'label' => $this->l('Display on the invoice:'),
                        'desc' =>
                            $this->l('Activate or deactivate the display of all the carriers used on the invoice'),
                        'values' => array(
                            array(
                                'id' => 'active_invoice',
                                'value' => true,
                                'label' => $this->l('Yes'),
                            ),
                            array(
                                'id' => 'inactive_invoice',
                                'value' => false,
                                'label' => $this->l('No'),
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'MTB_display_order_details',
                        'label' => $this->l('Display on the order details of the customer:'),
                        'desc' =>
                            $this->l('Activate or deactivate the display of all the carriers used on the order details of the customer'),
                        'values' => array(
                            array(
                                'id' => 'active_order_details',
                                'value' => true,
                            ),
                            array(
                                'id' => 'inactive_order_details',
                                'value' => false,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'MTB_display_mail',
                        'label' => $this->l('Display on the mail:'),
                        'desc' =>
                            $this->l('Activate or deactivate the display of all the carriers used on the mail'),
                        'values' => array(
                            array(
                                'id' => 'active_mail',
                                'value' => true,
                            ),
                            array(
                                'id' => 'inactive_mail',
                                'value' => false,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'MTB_show_all_carriers',
                        'label' => $this->l('Show all carriers:'),
                        'desc' => $this->l('Allow delivery with carrier not compatible with the location of the order.'),
                        'values' => array(
                            array(
                                'id' => 'active_all_carriers',
                                'value' => true,
                            ),
                            array(
                                'id' => 'inactive_all_carriers',
                                'value' => false,
                            ),
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'MTB_show_all_products',
                        'label' => $this->l('Show all products for a carrier:'),
                        'desc' => $this->l('Allow delivery with carrier not compatible with a product.'),
                        'values' => array(
                            array(
                                'id' => 'active_all_products',
                                'value' => true,
                            ),
                            array(
                                'id' => 'inactive_all_products',
                                'value' => false,
                            ),
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'PS_ORDER_RECALCULATE_SHIPPING',
                        'label' => $this->l('Recalculate the shipping price on order\'s update:'),
                        'values' => array(
                            array(
                                'id' => 'active_recalculate',
                                'value' => true,
                            ),
                            array(
                                'id' => 'inactive_recalculate',
                                'value' => false,
                            ),
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'MTB_NO_PRODUCTS',
                        'label' => $this->l('No restriction on the products to be delivered'),
                        'desc' => $this->l('You can select no product or several times the same one'),
                        'values' => array(
                            array(
                                'id' => 'active_no_products',
                                'value' => true,
                            ),
                            array(
                                'id' => 'inactive_no_products',
                                'value' => false,
                            ),
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'MTB_SEND_EMAIL',
                        'label' => $this->l('Send email on each shipping change'),
                        'desc' => $this->l('You can edit this behavior directly from the shipping popup'),
                        'values' => array(
                            array(
                                'id' => 'active_send_email',
                                'value' => true,
                            ),
                            array(
                                'id' => 'inactive_send_email',
                                'value' => false,
                            ),
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );

        return $form;
    }

    protected function postProcess()
    {
        Configuration::updateValue('MTB_display_invoice', (int)Tools::getValue('MTB_display_invoice'));
        Configuration::updateValue('MTB_display_order_details', (int)Tools::getValue('MTB_display_order_details'));
        Configuration::updateValue('MTB_display_mail', (int)Tools::getValue('MTB_display_mail'));
        Configuration::updateValue('MTB_show_all_carriers', (int)Tools::getValue('MTB_show_all_carriers'));
        Configuration::updateValue('MTB_show_all_products', (int)Tools::getValue('MTB_show_all_products'));
        Configuration::updateValue(
            'PS_ORDER_RECALCULATE_SHIPPING',
            (int)Tools::getValue('PS_ORDER_RECALCULATE_SHIPPING')
        );
        Configuration::updateValue(
            'MTB_NO_PRODUCTS',
            (int)Tools::getValue('MTB_NO_PRODUCTS')
        );
        Configuration::updateValue(
            'MTB_SEND_EMAIL',
            (int)Tools::getValue('MTB_SEND_EMAIL')
        );
    }

    public function hookDisplayAdminOrderTabShip($params)
    {
        return $this->context->smarty->fetch($this->getTemplatePath('displayAdminOrderTabShip.tpl'));
    }

    public function hookDisplayBackOfficeHeader()
    {
        if (!Module::isEnabled($this->name)) {
            return false;
        }

        return '<script>var admin_multitrackingbo_ajax_url = ' . (string) json_encode(
            pSQL($this->context->link->getAdminLink('AdminMultiTrackingBo'))
        ) . ';</script>';
    }

    public function hookDisplayAdminOrderContentShip($params)
    {
        $order = $params['order'];

        $id_zone = Address::getZoneById($order->id_address_delivery);
        $carrier_list = Carrier::getCarriers(
            $this->context->language->id,
            true,
            false,
            Configuration::get('MTB_show_all_carriers') ? false : $id_zone,
            null,
            Carrier::ALL_CARRIERS
        );

        $order_products = $order->getProducts();

        // TODO undisplay "add" button (if all products of the order has a carrier)
        $this->context->smarty->assign(array(
            'order' => $order,
            'currency' => new Currency($order->id_currency),
            'carrier_list' => $carrier_list,
            'order_products' => $order_products,
            'order_shipping' => MTB::getShipping($order),
            'recalculate' => (bool)Configuration::get('PS_ORDER_RECALCULATE_SHIPPING'),
            'mtb_invoices' => $order->getInvoicesCollection()->getResults(),
            'mtb_lang_id' => (int)$this->context->language->id,
            'send_email' => Configuration::get('MTB_SEND_EMAIL')
        ));

        return $this->context->smarty->fetch($this->getTemplatePath('displayAdminOrderContentShip.tpl'));
    }

    public function hookDisplayAdminOrderTabLink($params)
    {
        $id_order = (int)$params['id_order'];
        $order = new Order($id_order);
        $order_carrier = MTB::getShipping($order);

        $this->context->smarty->assign(array(
            'shipping_count' => (int)count($order_carrier)
        ));
        return $this->context->smarty->fetch($this->getTemplatePath('displayAdminOrderTabLink.tpl'));
    }

    public function hookDisplayAdminOrderTabContent($params)
    {
        $id_order = (int)$params['id_order'];
        $order = new Order($id_order);

        $id_zone = Address::getZoneById($order->id_address_delivery);
        $carrier_list = Carrier::getCarriers(
            $this->context->language->id,
            true,
            false,
            Configuration::get('MTB_show_all_carriers') ? false : $id_zone,
            null,
            Carrier::ALL_CARRIERS
        );

        $order_products = $order->getProducts();

        $this->context->smarty->assign(array(
            'order' => $order,
            'currency' => new Currency($order->id_currency),
            'carrier_list' => $carrier_list,
            'order_products' => $order_products,
            'order_shipping' => MTB::getShipping($order),
            'recalculate' => (bool)Configuration::get('PS_ORDER_RECALCULATE_SHIPPING'),
            'mtb_invoices' => $order->getInvoicesCollection()->getResults(),
            'mtb_lang_id' => (int)$this->context->language->id,
            'send_email' => Configuration::get('MTB_SEND_EMAIL')
        ));

        return $this->context->smarty->fetch($this->getTemplatePath('displayAdminOrderTabContent.tpl'));
    }

    public function hookDisplayPDFInvoice($params)
    {
        if (!Configuration::get('MTB_display_invoice')) {
            return '';
        }

        $order = $params['object'];

        $products = Db::getInstance()->executeS(
            "SELECT `a`.`quantity` AS 'product_quantity', `od`.`product_name`, `od`.`product_reference`
            FROM `" . _DB_PREFIX_ . "multitrackingbo_products` `a`
            LEFT JOIN `" . _DB_PREFIX_ . "order_carrier` oc
                ON `a`.`id_order_carrier` = `oc`.`id_order_carrier`
            LEFT JOIN `" . _DB_PREFIX_ . "carrier` c
                ON `oc`.`id_carrier` = `c`.`id_carrier`
            LEFT JOIN `" . _DB_PREFIX_ . "order_detail` od
                ON `oc`.`id_order` = `od`.`id_order`
                AND `a`.`id_product` = `od`.`product_id`
                AND `a`.`id_product_attribute` = `od`.`product_attribute_id`
            WHERE `oc`.`id_order` = " . (int)$order->id_order . "
            ORDER BY `a`.`id_order_carrier`"
        );

        $carriers = Db::getInstance()->executeS(
            "SELECT COUNT(*) AS 'count', `c`.`name`
            FROM `" . _DB_PREFIX_ . "multitrackingbo_products` `a`
            LEFT JOIN `" . _DB_PREFIX_ . "order_carrier` `oc`
                ON `a`.`id_order_carrier` = `oc`.`id_order_carrier`
            LEFT JOIN `" . _DB_PREFIX_ . "carrier` c
                ON `oc`.`id_carrier` = `c`.`id_carrier`
            WHERE `oc`.`id_order` = " . (int)$order->id_order . "
            GROUP BY `a`.`id_order_carrier`
            ORDER BY `a`.`id_order_carrier`"
        );

        foreach ($carriers as &$carrier) {
            if (version_compare(_PS_VERSION_, '8.0.0', '<')) {
                $carrier['name'] = Cart::replaceZeroByShopName($carrier['name'], null);
            }
        }

        if (empty($products)) {
            return '';
        }

        $this->context->smarty->assign(array(
            'order_carriers' => $products,
            'nb_products_per_carrier' => $carriers
        ));

        try {
            return $this->context->smarty->fetch($this->getTemplatePath('displayPdfInvoice.tpl'));
        } catch (Exception $e) {
            return '';
        }
    }

    public function hookDisplayOrderDetail($params)
    {
        if (!Configuration::get('MTB_display_order_details')) {
            return '';
        }

        $order = $params['order'];

        $products = Db::getInstance()->executeS(
            "SELECT `a`.`quantity` AS 'product_quantity', `od`.`product_name`, `od`.`product_reference`,
                `c`.`name` AS 'carrier_name'
            FROM `" . _DB_PREFIX_ . "multitrackingbo_products` `a`
            LEFT JOIN `" . _DB_PREFIX_ . "order_carrier` oc
                ON `a`.`id_order_carrier` = `oc`.`id_order_carrier`
            LEFT JOIN `" . _DB_PREFIX_ . "carrier` c
                ON `oc`.`id_carrier` = `c`.`id_carrier`
            LEFT JOIN `" . _DB_PREFIX_ . "order_detail` od
                ON `oc`.`id_order` = `od`.`id_order`
                AND `a`.`id_product` = `od`.`product_id`
                AND `a`.`id_product_attribute` = `od`.`product_attribute_id`
            WHERE `oc`.`id_order` = " . (int)$order->id . "
            ORDER BY `a`.`id_order_carrier`"
        );

        $carriers = Db::getInstance()->executeS(
            "SELECT COUNT(*) AS 'count', `c`.`name`, `cl`.`delay`
            FROM `" . _DB_PREFIX_ . "multitrackingbo_products` `a`
            LEFT JOIN `" . _DB_PREFIX_ . "order_carrier` `oc`
                ON `a`.`id_order_carrier` = `oc`.`id_order_carrier`
            LEFT JOIN `" . _DB_PREFIX_ . "carrier` c
                ON `oc`.`id_carrier` = `c`.`id_carrier`
            LEFT JOIN `" . _DB_PREFIX_ . "carrier_lang` `cl`
                ON `c`.`id_carrier` = `cl`.`id_carrier`
                AND `cl`.`id_lang` = " . (int)$this->context->language->id . "
                AND `cl`.`id_shop` = " . (int)$this->context->shop->id . "
            WHERE `oc`.`id_order` = " . (int)$order->id . "
            GROUP BY `a`.`id_order_carrier`
            ORDER BY `a`.`id_order_carrier`"
        );

        foreach ($products as &$product) {
            if (version_compare(_PS_VERSION_, '8.0.0', '<')) {
                $product['carrier_name'] = Cart::replaceZeroByShopName($product['carrier_name'], null);
            }
        }

        foreach ($carriers as &$carrier) {
            if (version_compare(_PS_VERSION_, '8.0.0', '<')) {
                $carrier['name'] = Cart::replaceZeroByShopName($carrier['name'], null);
            }
        }

        if (empty($products)) {
            return '';
        }

        $this->context->smarty->assign(array(
            'order_carriers' => $products,
            'nb_products_per_carrier' => $carriers
        ));

        try {
            return $this->context->smarty->fetch($this->getTemplatePath('displayOrderDetail.tpl'));
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function hookActionOrderDeliverySlipPdfForm($params)
    {
        $params['form_builder']->add('mtb', PrestaShopBundle\Form\Admin\Type\SwitchType::class, [
            'required' => false,
            'label' => $this->l('Multi carriers / tracking numbers')
        ]);
    }

    public function hookActionOrderDeliverySlipPdfSave($params)
    {
        if (isset($params['form_data']['mtb'])) {
            Configuration::updateValue('MTB_generateSlips', (bool)$params['form_data']['mtb']);
        } else {
            Configuration::updateValue('MTB_generateSlips', false);
        }
    }

    public function hookActionAdminDeliverySlipFormModifier($params)
    {
        $params['fields'][0]['form']['input'][] = array(
            'type' => 'switch',
            'label' => $this->l('Multi carriers / tracking numbers'),
            'name' => 'mtb',
            'is_bool' => true,
            'values' => array(
                array(
                    'id' => 'mtb_on',
                    'value' => 1,
                    'label' => $this->l('Yes')
                ),
                array(
                    'id' => 'mtb_off',
                    'value' => 0,
                    'label' => $this->l('No')
                )
            )
        );

        $params['fields_value']['mtb'] = 0;
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
        $order_state = $params['newOrderStatus'];
        $id_order = (int)$params['id_order'];

        if ($order_state->invoice) {
            Db::getInstance()->execute(
                "UPDATE `" . _DB_PREFIX_ . "multitrackingbo_invoices` `mtb`
                INNER JOIN `" . _DB_PREFIX_ . "order_carrier` `oc`
                    ON `mtb`.`id_order_carrier` = `oc`.`id_order_carrier`
                INNER JOIN `" . _DB_PREFIX_ . "order_invoice` `oi`
                    ON `oc`.`id_order` = `oi`.`id_order`
                SET `mtb`.`id_invoice` = `oi`.`id_order_invoice`
                WHERE `oi`.`id_order` = " . (int)$id_order . "
                    AND `mtb`.`id_invoice` = 0"
            );
        }

        return true;
    }
}
