<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'multitrackingbo_products` (
    `id_order_carrier` INT(11) NOT NULL,
    `id_product` INT(11) NOT NULL,
    `id_product_attribute` INT(11) NOT NULL,
    `quantity` INT(11) NOT NULL,
    PRIMARY KEY  (`id_order_carrier`, `id_product`, `id_product_attribute`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'multitrackingbo_invoices` (
    `id_order_carrier` INT(11) NOT NULL,
    `id_invoice` INT(11) NOT NULL,
    `number` INT(7) unsigned NOT NULL,
    `active` BOOLEAN DEFAULT TRUE,
    PRIMARY KEY  (`id_order_carrier`, `id_invoice`),
    UNIQUE (`id_invoice`, `number`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
