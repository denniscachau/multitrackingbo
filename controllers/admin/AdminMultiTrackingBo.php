<?php
/**
 * 2007-2023 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2023 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

include_once _PS_MODULE_DIR_ . 'multitrackingbo/classes/MTB.php';
include_once _PS_MODULE_DIR_ . 'multitrackingbo/classes/MTBOrderInvoice.php';
include_once _PS_MODULE_DIR_ . 'multitrackingbo/classes/HTMLTemplateMtbDeliverySlip.php';

class AdminMultiTrackingBoController extends ModuleAdminController
{
    public function ajaxProcessAddTracking()
    {
        $order_carrier = new OrderCarrier();
        $id_order = (int)Tools::getValue('id_order');
        $id_carrier = (int)Tools::getValue('id_carrier');
        $id_invoice = (int)Tools::getValue('id_invoice');
        $order = new Order($id_order);

        $products = $order->getProducts();

        $order_carrier->id_order = $id_order;
        $order_carrier->id_carrier = $id_carrier;
        $order_carrier->id_order_invoice = 0;
        $order_carrier->shipping_cost_tax_excl = 0;
        $order_carrier->shipping_cost_tax_incl = 0;
        $order_carrier->tracking_number = pSQL(Tools::getValue('tracking_number'));

        $id_order_carrier = 0;

        $id_products = array();
        $sql = array();
        foreach ($products as $product) {
            $id_product = (int)$product['product_id'];
            $id_product_attribute = (int)$product['product_attribute_id'];
            $quantity = (int)Tools::getValue('mtb_select_' . $id_product . '_' . $id_product_attribute, 0);

            if ($quantity > 0) {
                if (!Configuration::get('MTB_NO_PRODUCTS')) {
                    $has_no_duplicate = MTB::hasNoDuplicate(
                        $id_order_carrier,
                        $id_order,
                        $products,
                        array(
                            'id_product' => $id_product,
                            'id_product_attribute' => $id_product_attribute,
                            'quantity' => $quantity
                        )
                    );

                    if (!$has_no_duplicate) {
                        die(json_encode(array(
                            'success' => 0,
                            'text' => $this->l('A product is duplicated') . ' (' . $product['product_name'] . ')'
                        )));
                    }
                }

                $sql[] = array(
                    'id_product' => $id_product,
                    'id_product_attribute' => $id_product_attribute,
                    'quantity' => $quantity
                );
                for ($i = 0; $i < $quantity; $i++) {
                    $id_products[] = $id_product . ';'. $id_product_attribute;
                }
            }
        }

        if (empty($id_products) && !Configuration::get('MTB_NO_PRODUCTS')) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('Please add at least one product.')
            )));
        }

        try {
            $order_carrier->add();
        } catch (Exception $e) {
            die(json_encode(array(
                'success' => 0,
                'text' => $e->getMessage()
            )));
        }

        $id_order_carrier = (int)$order_carrier->id;

        foreach ($sql as $query) {
            $query['id_order_carrier'] = $id_order_carrier;
            Db::getInstance()->insert('multitrackingbo_products', $query);
        }

        $order_carrier->weight = MTB::getTotalWeight($id_products, Tools::getValue('weight'));

        try {
            $order_carrier->update();
        } catch (Exception $e) {
            die(json_encode(array(
                'success' => 0,
                'text' => $e->getMessage()
            )));
        }

        if ($id_invoice > 0 && !MTB::generateDeliverySlipDb($id_order_carrier, $id_invoice)) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('An error has occurred.')
            )));
        }

        if (!MTB::refreshShippingCost($order, $order_carrier, Tools::getValue('shipping_price'))) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('An error has occurred.')
            )));
        }

        if (Tools::getValue('send_mail')) {
            if (!$this->sendInTransitEmail($order_carrier, $order)) {
                die(json_encode(array(
                    'success' => 0,
                    'text' => $this->l('Failed to send the email.')
                )));
            }
        }

        die(json_encode(array(
            'success' => 1
        )));
    }

    public function ajaxProcessEditTracking()
    {
        $id_order_carrier = (int)Tools::getValue('id_order_carrier');
        $tracking_number = Tools::getValue('tracking_number');
        if ($id_order_carrier == 0) {
            die(json_encode(array(
                'success' => '0',
                'text' => $this->l('An error has occurred.')
            )));
        } elseif (!empty($tracking_number) && !Validate::isTrackingNumber($tracking_number)) {
            die(json_encode(array(
                'success' => '0',
                'text' => $this->l('Bad tracking number.')
            )));
        }

        $order_carrier = new OrderCarrier($id_order_carrier);

        if (!Validate::isLoadedObject($order_carrier)) {
            die(json_encode(array(
                'success' => '0',
                'text' => $this->l('The shipping does not exist.')
            )));
        }

        $id_order = (int)Tools::getValue('id_order');
        $id_carrier = (int)Tools::getValue('id_carrier');
        $id_invoice = (int)Tools::getValue('id_invoice');
        $is_default = (int)Tools::getValue('is_default');
        $order = new Order($id_order);

        if (!Validate::isLoadedObject($order)) {
            die(json_encode(array(
                'success' => '0',
                'text' => $this->l('The order is not recognized.')
            )));
        }

        $products = $order->getProducts();

        $order_carrier->id_order = $id_order;
        $order_carrier->id_carrier = $id_carrier;
        $order_carrier->id_order_invoice = 0;
//        $order_carrier->shipping_cost_tax_excl = 0;
//        $order_carrier->shipping_cost_tax_incl = 0;
        $order_carrier->tracking_number = $tracking_number;

        $id_products = array();
        $sql = array();
        foreach ($products as $product) {
            $id_product = (int)$product['product_id'];
            $id_product_attribute = (int)$product['product_attribute_id'];
            $quantity = (int)Tools::getValue('mtb_select_' . $id_product . '_' . $id_product_attribute, 0);

            if ($quantity > 0) {
                if (!Configuration::get('MTB_NO_PRODUCTS')) {
                    $has_no_duplicate = MTB::hasNoDuplicate(
                        $id_order_carrier,
                        $id_order,
                        $products,
                        array(
                            'id_product' => $id_product,
                            'id_product_attribute' => $id_product_attribute,
                            'quantity' => $quantity
                        )
                    );

                    if (!$has_no_duplicate) {
                        die(json_encode(array(
                            'success' => 0,
                            'text' => $this->l('A product is duplicated') . ' (' . $product['product_name'] . ')'
                        )));
                    }
                }

                $sql[] = array(
                    'id_order_carrier' => $id_order_carrier,
                    'id_product' => $id_product,
                    'id_product_attribute' => $id_product_attribute,
                    'quantity' => $quantity
                );
                for ($i = 0; $i < $quantity; $i++) {
                    $id_products[] = $id_product . ';' . $id_product_attribute;
                }
            }
        }

        if (empty($id_products) && !Configuration::get('MTB_NO_PRODUCTS')) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('Please add at least one product.')
            )));
        }

        $where = '`id_order_carrier` = "' . $id_order_carrier . '"';
        Db::getInstance()->delete('multitrackingbo_products', $where);

        foreach ($sql as $query) {
            Db::getInstance()->insert('multitrackingbo_products', $query);
        }

        $order_carrier->weight = MTB::getTotalWeight($id_products, Tools::getValue('weight'));

        try {
            $order_carrier->update();
        } catch (Exception $e) {
            die(json_encode(array(
                'success' => 0,
                'text' => $e->getMessage()
            )));
        }

        if ($is_default) {
            $order->id_carrier = (int)$id_carrier;
            $order->shipping_number = pSQL(Tools::getValue('tracking_number'));

            try {
                $order->update();
            } catch (Exception $e) {
                die(json_encode(array(
                    'success' => 0,
                    'text' => $e->getMessage()
                )));
            }
        }

        if ($id_invoice > 0 && !MTB::generateDeliverySlipDb($id_order_carrier, $id_invoice)) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('An error has occurred.')
            )));
        }

        if (!MTB::refreshShippingCost($order, $order_carrier, Tools::getValue('shipping_price'))) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('An error has occurred.')
            )));
        }

        if (Tools::getValue('send_mail')) {
            if (!$this->sendInTransitEmail($order_carrier, $order)) {
                die(json_encode(array(
                    'success' => 0,
                    'text' => $this->l('Failed to send the email.')
                )));
            }
        }

        die(json_encode(array(
            'success' => 1
        )));
    }

    public function ajaxProcessRecoverProductsByCarrier()
    {
        $id_carrier = (int)Tools::getValue('id_carrier');
        $id_order = (int)Tools::getValue('id_order');
        $id_order_carrier = (int)Tools::getValue('id_order_carrier');
        $show_all_products = (bool)Configuration::get('MTB_show_all_products');

        $order = new Order($id_order);

        if (!Validate::isLoadedObject($order)) {
            die(json_encode(array(
                'success' => '0',
                'text' => $this->l('The order is not recognized.')
            )));
        }

        $available_products = array();

        // For each product, check if it's compatible with the selected carrier
        foreach ($order->getProducts() as $order_product) {
            $add_product = false;
            $id_product = (int)$order_product['product_id'];
            $id_product_attribute = (int)$order_product['product_attribute_id'];
            $product = new Product($id_product);

            if (!$show_all_products && !empty($carriers = $product->getCarriers())) {
                foreach ($carriers as $carrier) {
                    if ($carrier['id_carrier'] == $id_carrier) {
                        $add_product = true;
                        break;
                    }
                }
            } else {
                $add_product = true;
            }

            if ($add_product) {
                $quantity = (int)MTB::isProductUseCarrier(
                    $id_order_carrier,
                    $id_product,
                    $id_product_attribute
                );
                $available_products[] = array(
                    'id_product' => $id_product,
                    'id_product_attribute' => $id_product_attribute,
                    'name' => pSQL($product->name[$this->context->language->id]),
                    'quantity' => (int)$order_product['product_quantity'],
                    'checked' => $quantity
                );
            }
        }

        die(json_encode(array('success' => 1, 'products' => $available_products)));
    }

    public function ajaxProcessDeleteOrderCarrier()
    {
        $id_order_carrier = (int)Tools::getValue('id_order_carrier');
        $order_carrier = new OrderCarrier($id_order_carrier);
        $order = new Order($order_carrier->id_order);

        if (!$order_carrier->delete()) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('An error has occurred.')
            )));
        }

        Db::getInstance()->delete(
            'multitrackingbo_products',
            '`id_order_carrier` = ' . (int)$id_order_carrier
        );

        Db::getInstance()->delete(
            'multitrackingbo_invoices',
            '`id_order_carrier` = ' . (int)$id_order_carrier
        );

        if (!MTB::deleteShippingCost($order)) {
            die(json_encode(array(
                'success' => 0,
                'text' => $this->l('An error has occurred.')
            )));
        }

        die(json_encode(array(
            'success' => 1
        )));
    }

    public function sendInTransitEmail($order_carrier, $order)
    {
        $customer = new Customer((int) $order->id_customer);
        $carrier = new Carrier((int) $order_carrier->id_carrier, $order->id_lang);
        $address = new Address((int) $order->id_address_delivery);

        $metadata = '';

        if (Configuration::get('MTB_display_mail')) {
            $products = Db::getInstance()->executeS(
                "SELECT `od`.`product_id`, `od`.`product_attribute_id`, `a`.`quantity`, `od`.`product_name`,
                `od`.`product_reference`
            FROM `" . _DB_PREFIX_ . "multitrackingbo_products` `a`
            LEFT JOIN `" . _DB_PREFIX_ . "order_carrier` oc
                ON `a`.`id_order_carrier` = `oc`.`id_order_carrier`
            LEFT JOIN `" . _DB_PREFIX_ . "order_detail` od
                ON `oc`.`id_order` = `od`.`id_order`
                AND `a`.`id_product` = `od`.`product_id`
                AND `a`.`id_product_attribute` = `od`.`product_attribute_id`
            WHERE `a`.`id_order_carrier` = " . (int)$order_carrier->id
            );

            $this->context->smarty->assign(array(
                'products' => $products
            ));
            $metadata = $this->context->smarty->fetch($this->getTemplatePath() . 'displayInTransitMail.tpl');
        }

        $templateVars = array(
            '{followup}' => str_replace('@', $order_carrier->tracking_number, $carrier->url),
            '{firstname}' => $customer->firstname,
            '{lastname}' => $customer->lastname,
            '{id_order}' => $order->id,
            '{shipping_number}' => $order_carrier->tracking_number,
            '{order_name}' => $order->getUniqReference(),
            '{carrier}' => $carrier->name,
            '{address1}' => $address->address1,
            '{country}' => $address->country,
            '{postcode}' => $address->postcode,
            '{city}' => $address->city,
            '{meta_products}' => $metadata,
        );

        if (Mail::Send(
            (int) $order->id_lang,
            'in_transit',
            $this->l('Package in transit'),
            $templateVars,
            $customer->email,
            $customer->firstname . ' ' . $customer->lastname,
            null,
            null,
            null,
            null,
            _PS_MAIL_DIR_,
            true,
            (int) $order->id_shop
        )) {
            return true;
        } else {
            return false;
        }
    }

    public function processGenerateDeliverySlipPDF()
    {
        $id_order_invoice = Tools::getValue('id_order_invoice');
        $order_invoice = new MTBOrderInvoice((int) $id_order_invoice);
        if (!Validate::isLoadedObject($order_invoice)) {
            throw new PrestaShopException('Can\'t load Order Invoice object');
        }

        $order_invoice->id_order_carrier = (int)Tools::getValue('id_order_carrier');

        $pdf = new PDF($order_invoice, 'MtbDeliverySlip', Context::getContext()->smarty);
        $pdf->render();
    }
}
