{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<style>
    td.color_line_even {
        background-color: #FFFFFF;
    }

    td.color_line_odd {
        background-color: #F9F9F9;
    }

    td.mtb_shipping {
        height: 20px;
        vertical-align: middle;
        font-size: 9pt;
    }

    table.mtb_shipping {
        border: 1px solid #000000;
        border-collapse: collapse;
        padding: 4px;
    }

    th.mtb_shipping {
        border-bottom: 1px solid #000000;
    }

    tr.mtb_shipping td {
        border-bottom: 1px solid #CCCCCC;
        vertical-align: middle;
    }
</style>

<table class="mtb_shipping">
    <thead>
        <tr>
            <th class="mtb_shipping header small">{l s='Carrier' mod='multitrackingbo'}</th>
            <th class="mtb_shipping header small">{l s='Product' mod='multitrackingbo'}</th>
            <th class="mtb_shipping header small">{l s='Qty' mod='multitrackingbo'}</th>
        </tr>
    </thead>

    <tbody>
    {assign var="loop_global" value=0}
    {foreach $nb_products_per_carrier as $carrier}
        {cycle values=["color_line_even", "color_line_odd"] assign=bgcolor_carrier_class}
        <tr class="mtb_shipping {$bgcolor_carrier_class|escape:'htmlall':'UTF-8'}">
            <td rowspan="{$carrier.count|intval}" class="product left {$bgcolor_carrier_class|escape:'htmlall':'UTF-8'}">
                {$carrier.name|escape:'htmlall':'UTF-8'}
            </td>
            {assign var="tr" value=0}
            {for $loop=$loop_global to ($loop_global + $carrier['count'] - 1)}
                {if $loop % 2}
                    {assign var="bgcolor_products_class" value="color_line_odd"}
                {else}
                    {assign var="bgcolor_products_class" value="color_line_even"}
                {/if}
                {if $tr == 1}
                    <tr>
                {else}
                    {assign var="tr" value=1}
                {/if}
                    <td class="mtb_shipping center {$bgcolor_products_class|escape:'htmlall':'UTF-8'}">
                        {$order_carriers[$loop].product_name|escape:'htmlall':'UTF-8'}
                    </td>
                    <td class="mtb_shipping center {$bgcolor_products_class|escape:'htmlall':'UTF-8'}">
                        {$order_carriers[$loop].product_quantity|intval}
                    </td>
                </tr>
            {/for}
        {assign var="loop_global" value=($loop_global + $carrier['count'])}
    {/foreach}
    </tbody>
</table>
