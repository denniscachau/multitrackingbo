{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="tab-pane active" id="multitrackingbo">
    <h4 class="visible-print">
        {l s='Shipping' mod='multitrackingbo'}
    </h4>

    {if !$order->isVirtual()}
        <div class="form-horizontal">
            {if $order->gift_message}
                <div class="form-group">
                    <label class="control-label col-lg-3">{l s='Message' mod='multitrackingbo'}</label>
                    <div class="col-lg-9">
                        <p class="form-control-static">{$order->gift_message|nl2br|escape:'htmlall':'UTF-8'}</p>
                    </div>
                </div>
            {/if}

            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <span class="title_box ">{l s='Date' mod='multitrackingbo'}</span>
                        </th>
                        <th>
                            <span class="title_box ">&nbsp;</span>
                        </th>
                        <th>
                            <span class="title_box ">{l s='Carrier' mod='multitrackingbo'}</span>
                        </th>
                        <th>
                            <span class="title_box ">{l s='Weight' mod='multitrackingbo'}</span>
                        </th>
                        <th>
                            <span class="title_box ">{l s='Shipping cost' mod='multitrackingbo'}</span>
                        </th>
                        <th>
                            <span class="title_box ">{l s='Tracking number' mod='multitrackingbo'}</span>
                        </th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$order_shipping item=line key=key}
                        <tr>
                            <td>{dateFormat date=$line.date_add full=true}</td>
                            <td>&nbsp;</td>
                            <td>{$line.carrier_name|escape:'htmlall':'UTF-8'}</td>
                            <td class="weight">{$line.weight|string_format:"%.3f"|escape:'htmlall':'UTF-8'} {Configuration::get('PS_WEIGHT_UNIT')|escape:'htmlall':'UTF-8'}</td>
                            <td class="price_carrier_{$line.id_carrier|intval} center">
                                <span>
                                {if $order->getTaxCalculationMethod() == $smarty.const.PS_TAX_INC}
                                    {displayPrice price=$line.shipping_cost_tax_incl currency=$currency->id}
                                {else}
                                    {displayPrice price=$line.shipping_cost_tax_excl currency=$currency->id}
                                {/if}
                                </span>
                            </td>
                            <td>
                                <span class="shipping_number_show">{if $line.url && $line.tracking_number}<a class="_blank" href="{$line.url|replace:'@':$line.tracking_number|escape:'htmlall':'UTF-8'}">{$line.tracking_number|escape:'htmlall':'UTF-8'}</a>{else}{$line.tracking_number|escape:'htmlall':'UTF-8'}{/if}</span>
                            </td>
                            <td>
                                {if $line.can_edit}
{*                                    <a href="#" class="multitrackingbo_link edit btn btn-default"*}
{*                                       data-id-order-carrier="{$line.id_order_carrier|intval}"*}
{*                                       data-id-carrier="{$line.id_carrier|intval}"*}
{*                                       data-tracking-number="{$line.tracking_number|escape:'htmlall':'UTF-8'}"*}
{*                                    >*}
{*                                        <i class="icon-pencil"></i>*}
{*                                        {l s='Edit' mod='multitrackingbo'}*}
{*                                    </a>*}

{*                                    {if $key != 0}*}
{*                                        <a href="{$link->getAdminLink('AdminOrders', true)|escape:'html':'UTF-8'}&amp;id_order={$order->id|intval}&amp;vieworder&amp;conf=1" class="multitrackingbo_delete btn btn-danger"*}
{*                                           data-id-order-carrier="{$line.id_order_carrier|intval}"*}
{*                                        >*}
{*                                            <i class="icon-trash"></i>*}
{*                                            {l s='Delete' mod='multitrackingbo'}*}
{*                                        </a>*}
{*                                    {/if}*}

{*                                    {if $line.id_invoice}*}
{*                                        <a class="btn btn-success _blank" href="{$link->getAdminLink('AdminMultiTrackingBo', true)|escape:'html':'UTF-8'}&amp;id_order_invoice={$line.id_invoice|intval}&amp;id_order_carrier={$line.id_order_carrier|intval}&amp;action=generateDeliverySlipPDF">Dl Slip</a>*}
{*                                    {/if}*}


                                    <div class="btn-group-action">
                                        <div class="btn-group pull-right">
                                            <a href="#" class="multitrackingbo_link edit btn btn-default"
                                               data-id-order-carrier="{$line.id_order_carrier|intval}"
                                               data-id-carrier="{$line.id_carrier|intval}"
                                               data-tracking-number="{$line.tracking_number|escape:'htmlall':'UTF-8'}"
                                               data-id-invoice="{$line.id_invoice|intval}"
                                               data-is-default="{$key == 0|intval}"
                                            >
                                                <i class="icon-pencil"></i>
                                                {l s='Edit' mod='multitrackingbo'}
                                            </a>

                                            {if $key != 0 || $line.id_invoice}
                                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-caret-down"></i>&nbsp;
                                                </button>

                                                <ul class="dropdown-menu">
                                                    {if $line.id_invoice}
                                                        <li>
                                                            <a class="_blank" href="{$link->getAdminLink('AdminMultiTrackingBo', true)|escape:'html':'UTF-8'}&amp;id_order_invoice={$line.id_invoice|intval}&amp;id_order_carrier={$line.id_order_carrier|intval}&amp;action=generateDeliverySlipPDF">
                                                                <i class="icon-file-text"></i>
                                                                {l s='Delivery slip' mod='multitrackingbo'}
                                                            </a>
                                                        </li>
                                                    {/if}
                                                    {if $key != 0}
                                                        <li>
                                                            <a href="{$link->getAdminLink('AdminOrders', true)|escape:'html':'UTF-8'}&amp;id_order={$order->id|intval}&amp;vieworder&amp;conf=1" class="multitrackingbo_delete"
                                                               data-id-order-carrier="{$line.id_order_carrier|intval}">
                                                                <i class="icon-trash"></i>
                                                                {l s='Delete' mod='multitrackingbo'}
                                                            </a>
                                                        </li>
                                                    {/if}
                                                </ul>
                                            {/if}
                                        </div>
                                    </div>


                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="pull-right">
                            <a href="#" class="multitrackingbo_link add btn btn-primary">
                                <i class="icon-pencil"></i>
                                {l s='Add' mod='multitrackingbo'}
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <!-- shipping update modal -->
                <div class="modal fade" id="modal-multitrackingbo">
                    <div class="modal-dialog">
                        <form method="post" action="{$link->getAdminLink('AdminOrders', true)|escape:'html':'UTF-8'}&amp;id_order={$order->id|intval}&amp;vieworder&amp;conf=4" class="formMultitrackingbo">
                            <input type="hidden" id="mtb_id_order_carrier" />
                            <input type="hidden" id="mtb_is_default" />
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="{l s='Close' mod='multitrackingbo'}"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">{l s='Edit shipping details' mod='multitrackingbo'}</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        {if !$recalculate}
                                            <div class="alert alert-info">
                                                {l s='Please note that carrier change will not recalculate your shipping costs, if you want to change this please visit Shop Parameters > Order Settings' mod='multitrackingbo'}
                                            </div>
                                        {/if}
                                        <div class="form-group">
                                            <div class="col-lg-5">{l s='Invoice' mod='multitrackingbo'}</div>
                                            <div class="col-lg-7">
                                                <select id="mtb_invoice_id">
                                                    {foreach from=$mtb_invoices item=mtb_invoice}
                                                        <option value="{$mtb_invoice->id|intval}">{$mtb_invoice->getInvoiceNumberFormatted($mtb_lang_id)|escape:'htmlall':'UTF-8'}</option>
                                                    {foreachelse}
                                                        <option value="0">
                                                            {l s='No Invoice available' mod='multitrackingbo'}
                                                        </option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-5">{l s='Tracking number' mod='multitrackingbo'}</div>
                                            <div class="col-lg-7"><input type="text" id="mtb_tracking_number" /></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-5">{l s='Carrier' mod='multitrackingbo'}</div>
                                            <div class="col-lg-7">
                                                <select id="mtb_shipping_carrier">
                                                    {foreach from=$carrier_list item=carrier}
                                                        <option value="{$carrier.id_carrier|intval}">{$carrier.name|escape:'html':'UTF-8'} {if isset($carrier.delay)}({$carrier.delay|escape:'html':'UTF-8'}){/if}</option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                        <hr />
                                        <div class="form-horizontal">
                                            <div id="mtb_order_products">
                                            </div>
                                        </div>
                                        <hr />
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label><input type="checkbox" id="mtb_mail" value="1"{if $send_email} checked{/if}>{l s='Send an email?' mod='multitrackingbo'}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">{l s='Cancel' mod='multitrackingbo'}</button>
                                    <button type="submit" class="btn btn-primary">{l s='Update' mod='multitrackingbo'}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    {/if}
</div>

<script type="text/javascript">
    $(document).on('click', '.multitrackingbo_link', function(e) {
        e.preventDefault();

        let id_carrier;

        if ($(this).hasClass('edit')) {
            $('.formMultitrackingbo').data('new', 0);

            id_carrier = $(this).data('id-carrier');

            $('#mtb_id_order_carrier').val($(this).data('id-order-carrier'));
            $('#mtb_shipping_carrier option[value=' + id_carrier + ']').prop('selected', true);
            $('#mtb_tracking_number').val($(this).data('tracking-number'));
            if ($(this).data('id-invoice') > 0) {
                $('#mtb_invoice_id').val($(this).data('id-invoice'));
            }
            $('#mtb_is_default').val($(this).data('is-default'));
        } else {
            $('.formMultitrackingbo').data('new', 1);

            $('#mtb_id_order_carrier').val('0');
            $('#mtb_shipping_carrier option:first').prop('selected', true);
            $('#mtb_tracking_number').val('');

            id_carrier = $('#mtb_shipping_carrier').val();
        }

        $('#modal-multitrackingbo').modal();

        ajaxProcessRecoverProductsByCarrier(id_carrier)
    });

    $(document).on('change', '#mtb_shipping_carrier', function() {
        ajaxProcessRecoverProductsByCarrier($(this).val());
    })

    function ajaxProcessRecoverProductsByCarrier(id_carrier) {
        $('#mtb_order_products *').remove();

        $.ajax({
            type: 'POST',
            url: admin_multitrackingbo_ajax_url,
            dataType: 'json',
            data: {
                controller : 'AdminMultiTrackingBo',
                action : 'recoverProductsByCarrier',
                ajax : true,
                id_carrier : id_carrier,
                id_order : '{$order->id|intval}',
                id_order_carrier : $('#mtb_id_order_carrier').val()
            },
            success: function(data) {
                $('#mtb_order_products *').remove();
                if (data.success == '1') {
                    let html = "";

                    $.each(data.products, function(index, product) {
                        html += '<div class="form-group">';
                        html += '<div class="col-sm-2">\n' +
                            '<select class="form-control fixed-width-xs mtb_select" data-id_product="' + product.id_product + '" data-id_product_attribute="' + product.id_product_attribute + '" id="mtb_select_' + product.id_product + '_' + product.id_product_attribute + '">\n';

                        for (let i = 0; i <= product.quantity; i++) {
                            html += '<option value="' + i + '" ' + (product.checked == i ? 'selected' : '') + '>' + i + '</option>\n';
                        }

                        html += '</select>\n' +
                            '</div>\n' +
                            '<label for="mtb_select_' + product.id_product + '_' + product.id_product_attribute + '" class="col-sm-10 control-label pull-left" style="text-align:left">' +
                            product.name +
                            '</label>';
                        html += '</div>';
                    });

                    $('#mtb_order_products').append(html);
                }
            }
        });
    }

    $(document).on('click', '.multitrackingbo_delete', function(e) {
        if (confirm("{l s='Are you sure you want to delete this order\'s carrier?' mod='multitrackingbo'}")) {
            $.ajax({
                type: 'POST',
                url: admin_multitrackingbo_ajax_url,
                dataType: 'json',
                async: false,
                data: {
                    action: 'deleteOrderCarrier',
                    controller: 'AdminMultiTrackingBo',
                    ajax : true,
                    id_order_carrier: $(this).data('id-order-carrier')
                },
                success: function (data) {
                    if (data.success == 0) {
                        $.growl.error({
                            title: "",
                            message: data.text
                        });
                        e.preventDefault();
                    }
                }
            });
        } else {
            e.preventDefault();
        }
    });

    $(document).on('submit', '.formMultitrackingbo', function(e){
        let is_new = $(this).data('new');

        let params = {
            controller : 'AdminMultiTrackingBo',
            ajax : true,
            async : false,
            id_carrier : $('#mtb_shipping_carrier').val(),
            tracking_number : $('#mtb_tracking_number').val(),
            id_order : '{$order->id|intval}',
            id_order_carrier : $('#mtb_id_order_carrier').val(),
            send_mail : ($('#mtb_mail').is(':checked') ? 1 : 0),
            id_invoice : $('#mtb_invoice_id').val(),
            is_default : $('#mtb_is_default').val()
        };

        $('.mtb_select').each(function() {
            params['mtb_select_' + $(this).data('id_product') + '_' + $(this).data('id_product_attribute')] = $(this).val();
        })
        if (is_new === 1) {
            params['action'] = 'addTracking';
        } else {
            params['action']  = 'editTracking';
        }
        $.ajax({
            type : 'POST',
            url : admin_multitrackingbo_ajax_url,
            dataType : 'json',
            async : false,
            data : params,
            success : function(data) {
                if (data.success == 0) {
                    $.growl.error({ title: "", message: data.text });
                    e.preventDefault();
                }
            }
        });
    })

    $(document).ready(function() {
        $('#shipping').remove();
    });
</script>
