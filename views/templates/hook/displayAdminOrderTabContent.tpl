{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if !$order->isVirtual() }
    {if $order->gift_message}
        <div class="row col-lg-12">
            <label>
                {l s='Gift message:' mod='multitrackingbo'}
            </label>
            <div id="gift-message" class="col-lg-9">
                {$order->gift_message|escape:'htmlall':'UTF-8'}
            </div>
        </div>
    {/if}

    <table class="table">
        <thead>
        <tr>
            <th>{l s='Date' mod='multitrackingbo'}</th>
            <th>&nbsp;</th>
            <th>{l s='Carrier' mod='multitrackingbo'}</th>
            <th>{l s='Weight' mod='multitrackingbo'}</th>
            <th>{l s='Shipping cost' mod='multitrackingbo'}</th>
            <th>{l s='Tracking number' mod='multitrackingbo'}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$order_shipping item=line key=key}
        <tr>
            <td>{dateFormat date=$line.date_add full=true}</td>
            <td>&nbsp;</td>
            <td>{$line.carrier_name|escape:'htmlall':'UTF-8'}</td>
            <td>
                {$line.weight|string_format:"%.3f"|escape:'htmlall':'UTF-8'} {Configuration::get('PS_WEIGHT_UNIT')|escape:'htmlall':'UTF-8'}
            </td>
            <td>
                <span>
                {if $order->getTaxCalculationMethod() == $smarty.const.PS_TAX_INC}
                    {displayPrice price=$line.shipping_cost_tax_incl currency=$currency->id}
                {else}
                    {displayPrice price=$line.shipping_cost_tax_excl currency=$currency->id}
                {/if}
                </span>
            <td>
                <span class="shipping_number_show">{if $line.url && $line.tracking_number}<a class="_blank" href="{$line.url|replace:'@':$line.tracking_number|escape:'htmlall':'UTF-8'}">{$line.tracking_number|escape:'htmlall':'UTF-8'}</a>{else}{$line.tracking_number|escape:'htmlall':'UTF-8'}{/if}
                </span>
            </td>
            <td class="text-right">
                {if $line.can_edit}
                    <div class="btn-group">
                        <!-- <a href="#" class="multitrackingbo_link edit btn btn-default"
                           data-id-order-carrier="{$line.id_order_carrier|intval}"
                           data-id-carrier="{$line.id_carrier|intval}"
                           data-tracking-number="{$line.tracking_number|escape:'htmlall':'UTF-8'}"
                           data-id-invoice="{$line.id_invoice|intval}"
                        >
                        </a> -->
                        <a class="multitrackingbo_link edit btn tooltip-link dropdown-item" href="#"
                            data-confirm-message=""
                            data-toggle="pstooltip"
                            data-placement="top"
                            data-original-title="{l s='Edit' mod='multitrackingbo'}"
                            data-clickable-row=""
                            data-id-order-carrier="{$line.id_order_carrier|intval}"
                            data-id-carrier="{$line.id_carrier|intval}"
                            data-tracking-number="{$line.tracking_number|escape:'htmlall':'UTF-8'}"
                            data-id-invoice="{$line.id_invoice|intval}"
                            data-is-default="{$key == 0|intval}">
                            <i class="material-icons">edit</i>
                        </a>

                        {if $key != 0 || $line.id_invoice}
                            {if $line.id_invoice}
                                <a class="_blank btn tooltip-link dropdown-item" href="{$link->getAdminLink('AdminMultiTrackingBo', true, [], ['id_order_invoice' => $line.id_invoice, 'id_order_carrier' => $line.id_order_carrier, 'action' => 'generateDeliverySlipPDF'])|escape:'html':'UTF-8'}"
                                    data-confirm-message=""
                                    data-toggle="pstooltip"
                                    data-placement="top"
                                    data-original-title="{l s='Delivery Slip' mod='multitrackingbo'}"
                                    data-clickable-row="">
                                    <i class="material-icons">receipt</i>
                                </a>
                            {/if}

                            {if $key != 0}
                                <a class="multitrackingbo_delete edit btn tooltip-link dropdown-item" href="{$link->getAdminLink('AdminOrders', true, [], ['id_order' => $order->id, 'vieworder' => 1])|escape:'html':'UTF-8'}"
                                    data-confirm-message=""
                                    data-toggle="pstooltip"
                                    data-placement="top"
                                    data-original-title="{l s='Delete' mod='multitrackingbo'}"
                                    data-clickable-row=""
                                    data-id-order-carrier="{$line.id_order_carrier|intval}">
                                    <i class="material-icons">delete</i>
                                </a>
                            {/if}
                        {/if}
                    </div>
                {/if}
            </td>
        </tr>
        {/foreach}
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="text-right">
                <a href="#" class="multitrackingbo_link add btn btn-primary">
                    <i class="material-icons">add</i>
                    {l s='Add' mod='multitrackingbo'}
                </a>
            </td>
        </tr>
        </tbody>
    </table>

    {if $order->recyclable}
        <span class="badge badge-success">{l s='Recycled packaging' mod='multitrackingbo'}</span>
    {/if}

    {if $order->gift}
    <span class="badge badge-success">{l s='Gift wrapping' mod='multitrackingbo'}</span>
    {/if}
{else}
    <p class="text-center mb-0">
        {l s='Shipping does not apply to virtual orders' mod='multitrackingbo'}
    </p>
{/if}

<div class="modal fade" id="modal-multitrackingbo">
    <div class="modal-dialog">
        <form method="post" action="{$link->getAdminLink('AdminOrders', true, [], ['id_order' => $order->id, 'vieworder' => '1'])|escape:'html':'UTF-8'}" class="formMultitrackingbo">
            <input type="hidden" id="mtb_id_order_carrier" />
            <input type="hidden" id="mtb_is_default" />
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{l s='Edit shipping details' mod='multitrackingbo'}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="{l s='Close' mod='multitrackingbo'}"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        {if !$recalculate}
                            <div class="alert alert-info">
                                {l s='Please note that carrier change will not recalculate your shipping costs, if you want to change this please visit Shop Parameters > Order Settings' mod='multitrackingbo'}
                            </div>
                        {/if}

                        <div class="form-group">
                            <label class="form-control-label" for="mtb_invoice_id">
                                {l s='Invoice' mod='multitrackingbo'}
                            </label>
                            <select id="mtb_invoice_id" class="custom-select">
                                {foreach from=$mtb_invoices item=mtb_invoice}
                                    <option value="{$mtb_invoice->id|intval}">{$mtb_invoice->getInvoiceNumberFormatted($mtb_lang_id)|escape:'htmlall':'UTF-8'}</option>
                                {foreachelse}
                                    <option value="0">
                                        {l s='No Invoice available' mod='multitrackingbo'}
                                    </option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="mtb_tracking_number">
                                {l s='Tracking number' mod='multitrackingbo'}
                            </label>
                            <input type="text" class="form-control" id="mtb_tracking_number" />
                        </div>

                        <div class="form-group">
                            <label class="form-control-label" for="mtb_shipping_carrier">
                                {l s='Carrier' mod='multitrackingbo'}
                            </label>
                            <select id="mtb_shipping_carrier" class="custom-select">
                                {foreach from=$carrier_list item=carrier}
                                    <option value="{$carrier.id_carrier|intval}">{$carrier.name|escape:'html':'UTF-8'} {if isset($carrier.delay)}({$carrier.delay|escape:'html':'UTF-8'}){/if}</option>
                                {/foreach}
                            </select>
                        </div>

                        <hr />
                        <div class="form-horizontal">
                            <div id="mtb_order_products">
                            </div>
                        </div>
                        <hr />

                        <div class="checkbox">
                            <label>
                                <input id="mtb_override_price" value="1" type="checkbox"> {l s='Override delivery price?' mod='multitrackingbo'}
                            </label>
                        </div>

                        <div class="form-group" id="mtb_price_container" style="display: none">
                            <label class="form-control-label" for="mtb_price">
                                {l s='Delivery price (Tax Incl.)' mod='multitrackingbo'}
                            </label>
                            <input type="number" class="form-control" id="mtb_price" step="0.01" value="0" />
                        </div>

                        <div class="checkbox">
                            <label>
                                <input id="mtb_override_weight" value="1" type="checkbox"> {l s='Override delivery weight?' mod='multitrackingbo'}
                            </label>
                        </div>

                        <div class="form-group" id="mtb_weight_container" style="display: none">
                            <label class="form-control-label" for="mtb_weight">
                                {l s='Delivery weight' mod='multitrackingbo'}
                            </label>
                            <input type="number" class="form-control" id="mtb_weight" step="0.01" value="0" />
                        </div>

                        <div class="checkbox">
                            <label>
                                <input id="mtb_mail" value="1" type="checkbox"{if $send_email} checked{/if}> {l s='Send an email?' mod='multitrackingbo'}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{l s='Cancel' mod='multitrackingbo'}</button>
                    <button type="submit" class="btn btn-primary">{l s='Update' mod='multitrackingbo'}</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $('#orderShippingTab').parent().remove();
    $(document).on('click', '.multitrackingbo_link', function(e) {
        e.preventDefault();

        let id_carrier;

        if ($(this).hasClass('edit')) {
            $('.formMultitrackingbo').data('new', 0);

            id_carrier = $(this).data('id-carrier');

            $('#mtb_id_order_carrier').val($(this).data('id-order-carrier'));
            $('#mtb_shipping_carrier option[value=' + id_carrier + ']').prop('selected', true);
            $('#mtb_tracking_number').val($(this).data('tracking-number'));
            if ($(this).data('id-invoice') > 0) {
                $('#mtb_invoice_id').val($(this).data('id-invoice'));
            }
            $('#mtb_is_default').val($(this).data('is-default'));
        } else {
            $('.formMultitrackingbo').data('new', 1);

            $('#mtb_id_order_carrier').val('0');
            $('#mtb_shipping_carrier option:first').prop('selected', true);
            $('#mtb_tracking_number').val('');

            id_carrier = $('#mtb_shipping_carrier').val();
        }

        $('#modal-multitrackingbo').modal();

        ajaxProcessRecoverProductsByCarrier(id_carrier)
    });

    $(document).on('change', '#mtb_shipping_carrier', function() {
        ajaxProcessRecoverProductsByCarrier($(this).val());
    })

    $(document).on('change', '#mtb_override_price', function(event) {
        if ($(this).is(':checked')) {
            $('#mtb_price_container').show();
        } else {
            $('#mtb_price_container').hide();
        }
    });

    $(document).on('change', '#mtb_override_weight', function(event) {
        if ($(this).is(':checked')) {
            $('#mtb_weight_container').show();
        } else {
            $('#mtb_weight_container').hide();
        }
    });

    function ajaxProcessRecoverProductsByCarrier(id_carrier) {
        $('#mtb_order_products *').remove();

        $.ajax({
            type: 'POST',
            url: admin_multitrackingbo_ajax_url,
            dataType: 'json',
            data: {
                controller : 'AdminMultiTrackingBo',
                action : 'recoverProductsByCarrier',
                ajax : true,
                id_carrier : id_carrier,
                id_order : '{$order->id|intval}',
                id_order_carrier : $('#mtb_id_order_carrier').val()
            },
            success: function(data) {
                $('#mtb_order_products *').remove();
                if (data.success == '1') {
                    let html = "";

                    $.each(data.products, function(index, product) {
                        html += '<div class="form-group row">';
                        html += '<div class="col-sm-2">\n' +
                            '<select class="custom-select fixed-width-xs mtb_select" data-id_product="' + product.id_product + '" data-id_product_attribute="' + product.id_product_attribute + '" id="mtb_select_' + product.id_product + '_' + product.id_product_attribute + '">\n';

                        for (let i = 0; i <= product.quantity; i++) {
                            html += '<option value="' + i + '" ' + (product.checked == i ? 'selected' : '') + '>' + i + '</option>\n';
                        }

                        html += '</select>\n' +
                            '</div>\n' +
                            '<label for="mtb_select_' + product.id_product + '_' + product.id_product_attribute + '" class="my-auto col-sm-10 control-label" style="text-align:left">' +
                            product.name +
                            '</label>';
                        html += '</div>';
                    });

                    $('#mtb_order_products').append(html);
                }
            }
        });
    }

    $(document).on('click', '.multitrackingbo_delete', function(e) {
        if (confirm("{l s='Are you sure you want to delete this order\'s carrier?' mod='multitrackingbo'}")) {
            $.ajax({
                type: 'POST',
                url: admin_multitrackingbo_ajax_url,
                dataType: 'json',
                async: false,
                data: {
                    action: 'deleteOrderCarrier',
                    controller: 'AdminMultiTrackingBo',
                    ajax : true,
                    id_order_carrier: $(this).data('id-order-carrier')
                },
                success: function (data) {
                    if (data.success == 0) {
                        window.showErrorMessage(data.text);
                        e.preventDefault();
                    } else {
                        window.showSuccessMessage(window.update_success_msg);
                    }
                }
            });
        } else {
            e.preventDefault();
        }
    });

    $(document).on('submit', '.formMultitrackingbo', function(e){
        let is_new = $(this).data('new');

        let params = {
            controller : 'AdminMultiTrackingBo',
            ajax : true,
            async : false,
            id_carrier : $('#mtb_shipping_carrier').val(),
            tracking_number : $('#mtb_tracking_number').val(),
            id_order : '{$order->id|intval}',
            id_order_carrier : $('#mtb_id_order_carrier').val(),
            send_mail : ($('#mtb_mail').is(':checked') ? 1 : 0),
            id_invoice : $('#mtb_invoice_id').val(),
            is_default : $('#mtb_is_default').val()
        };

        if ($('#mtb_override_price').is(':checked')) {
            params['shipping_price'] = $('#mtb_price').val();
        }

        if ($('#mtb_override_weight').is(':checked')) {
            params['weight'] = $('#mtb_weight').val();
        }

        $('.mtb_select').each(function() {
            params['mtb_select_' + $(this).data('id_product') + '_' + $(this).data('id_product_attribute')] = $(this).val();
        })
        if (is_new === 1) {
            params['action'] = 'addTracking';
        } else {
            params['action']  = 'editTracking';
        }
        e.preventDefault();
        $.ajax({
            type : 'POST',
            url : admin_multitrackingbo_ajax_url,
            dataType : 'json',
            async : false,
            data : params,
            success : function(data) {
                if (data.success == 0) {
                    window.showErrorMessage(data.text);
                } else {
                    window.showSuccessMessage(window.update_success_msg);
                    window.location.reload();
                }
            }
        });
    })

    $(document).ready(function() {
        $('#shipping').remove();
    });
</script>
