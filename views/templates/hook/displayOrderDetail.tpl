{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="box hidden-sm-down">
    <table id="order-mtb_shipping" class="table table-bordered">
        <thead class="thead-default">
        <tr>
            <th>{l s='Carrier' mod='multitrackingbo'}</th>
            <th>{l s='Product' mod='multitrackingbo'}</th>
            <th>{l s='Quantity' mod='multitrackingbo'}</th>
        </tr>
        </thead>
        {assign var="loop_global" value=0}
        {foreach $nb_products_per_carrier as $carrier}
            <tr>
                <td rowspan="{$carrier.count|intval}">
                    <b>{$carrier.name|escape:'htmlall':'UTF-8'}</b>
                    {if $carrier.delay != ""}<br /> {$carrier.delay|escape:'htmlall':'UTF-8'}{/if}
                </td>
                {assign var="tr" value=0}
                    {for $loop=$loop_global to ($loop_global + $carrier['count'] - 1)}
                    {if $tr == 1}
                        <tr>
                    {else}
                        {assign var="tr" value=1}
                    {/if}
                    <td>
                        <a>
                            {$order_carriers[$loop].product_name|escape:'htmlall':'UTF-8'}
                        </a>
                    </td>
                    <td>
                        {$order_carriers[$loop].product_quantity|intval}
                    </td>
                {/for}
            {assign var="loop_global" value=($loop_global + $carrier['count'])}
            </tr>
        {/foreach}
    </table>
</div>

<div class="order-items hidden-md-up box">
    {foreach $order_carriers as $order_carrier}
        <div class="order-item">
            <div class="row">
                <div class="col-sm-4 desc">
                    <div class="name">{$order_carrier.carrier_name|escape:'htmlall':'UTF-8'}</div>
                </div>
                <div class="col-sm-8 qty">
                    <div class="row">
                        <div class="col-xs-6 text-sm-left text-xs-left">
                            {$order_carrier.product_name|escape:'htmlall':'UTF-8'}
                        </div>
                        <div class="col-xs-2">
                            {$order_carrier.product_quantity|intval}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {/foreach}
</div>
