{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2023 PrestaShop SA
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}

{if !empty($products)}
    <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" align="left" width="100%">
        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="
                border-collapse:collapse;
                mso-table-lspace:
                0pt;mso-table-rspace:0pt;
                font-family:Open sans,Arial,sans-serif;
                font-size:14px;
                vertical-align: top;"
               width="100%">
            <tr>
                <td align="left" class="products-table" style="
                border-collapse:collapse;
                mso-table-lspace:0pt;
                mso-table-rspace:0pt;
                font-family:Open sans,Arial,sans-serif;
                background-color:#FDFDFD;
                color:#353943;
                font-size:0px;
                padding: 10px25px;
                padding-right:50px;
                padding-left:50px;
                word-break:break-word;"
                    bgcolor="#FDFDFD">
                    <table cellpadding="0" cellspacing="0" width="100%" border="0" style="
                border-collapse:collapse;
                mso-table-lspace:0pt;
                mso-table-rspace:0pt;
                cellspacing:0;
                color:#000000;
                font-family:Open sans,arial,sans-serif;
                font-size:14px;
                line-height:22px;
                table-layout:auto;
                width:100%;">
                        <colgroup>
                            <col span="1" style="width:25%;" width="25%">
                            <col span="1" style="width:60%;" width="60%">
                            <col span="1" style="width:15%;" width="15%">
                        </colgroup>
                        <tr>
                            <th bgcolor="#FDFDFD" style="
                font-family:Open sans,Arial,sans-serif;
                font-size:12px;
                background-color:#FDFDFD;
                color:#353943;
                font-weight:600;
                padding:10px 5px;
                border: 1px solid #DFDFDF;">{l s='Reference' mod='multitrackingbo'}</th>
                            <th bgcolor="#FDFDFD" style="
                    font-family:Open sans,Arial,sans-serif;
                    font-size: 12px;
                    background-color:#FDFDFD;
                    color: #353943;
                    font-weight:600;
                    padding:10px 5px;
                    border:1px solid #DFDFDF;">{l s='Product' mod='multitrackingbo'}</th>
                            <th bgcolor="#FDFDFD" style="
                font-family:Open sans,Arial,sans-serif;
                font-size:12px;
                background-color:#FDFDFD;
                color:#353943;
                font-weight:600;
                padding:10px 5px;
                border:1px solid #DFDFDF;">{l s='Quantity' mod='multitrackingbo'}</th>
                        </tr>
                        {foreach from=$products item=product}
                            <tr class="order_summary">
                                <td bgcolor="#FDFDFD" align="center" style="border-collapse:collapse;
                    mso-table-lspace:0pt;
                    mso-table-rspace:0pt;
                    font-family:Open sans,Arial,sans-serif;
                    background-color:#FDFDFD;
                    color:#353943;
                    font-size:14px;
                    padding:10px;
                    border:1px solid #DFDFDF;">{$product.product_reference|escape:'htmlall':'UTF-8'}</td>
                                <td bgcolor="#FDFDFD" align="center" style="border-collapse:collapse;
                    mso-table-lspace:0pt;
                    mso-table-rspace:0pt;
                    font-family:Open sans,Arial,sans-serif;
                    background-color:#FDFDFD;
                    color:#353943;
                    font-size:14px;
                    padding:10px;
                    border:1px solid #DFDFDF;">{$product.product_name|escape:'htmlall':'UTF-8'}</td>
                                <td bgcolor="#FDFDFD" align="center" style="border-collapse:collapse;
                    mso-table-lspace:0pt;
                    mso-table-rspace:0pt;
                    font-family:Open sans,Arial,sans-serif;
                    background-color:#FDFDFD;
                    color:#353943;
                    font-size:14px;
                    padding:10px;
                    border:1px solid #DFDFDF;">{$product.quantity|intval}</td>
                            </tr>
                        {/foreach}
                    </table>
                </td>
            </tr>
        </table>
    </div>
{/if}
