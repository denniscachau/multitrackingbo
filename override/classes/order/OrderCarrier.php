<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

class OrderCarrier extends OrderCarrierCore
{
    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        $this->webserviceParameters['associations'] = [
            'mtb_products' => ['resource' => 'mtb_product', 'setter' => false, 'virtual_entity' => true,
                'fields' => [
                    'product_id' => ['required' => true, 'xlink_resource' => 'products'],
                    'product_attribute_id' => ['required' => true],
                    'product_quantity' => ['required' => true],
                ],
            ],
        ];

        parent::__construct($id, $id_lang, $id_shop);
    }

    public function getWsMtbProducts()
    {
        $query = 'SELECT `id_product` AS `product_id`,
            `id_product_attribute` AS `product_attribute_id`,
            `quantity` AS `product_quantity`
            FROM `' . _DB_PREFIX_ . 'multitrackingbo_products`
            WHERE `id_order_carrier` = ' . (int) $this->id;

        return Db::getInstance()->executeS($query);
    }
}
