<?php
/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

class AdminDeliverySlipController extends AdminDeliverySlipControllerCore
{
    public function postProcess()
    {
        if (Tools::isSubmit('submitAdddelivery')) {
            if (!Validate::isDate(Tools::getValue('date_from'))) {
                $this->errors[] = $this->trans('Invalid \'from\' date', array(), 'Admin.Catalog.Notification');
            }
            if (!Validate::isDate(Tools::getValue('date_to'))) {
                $this->errors[] = $this->trans('Invalid \'to\' date', array(), 'Admin.Catalog.Notification');
            }
            if (!count($this->errors)) {
                Configuration::updateValue('MTB_generateSlips', (bool)Tools::getValue('mtb'));
                if (Tools::getValue('mtb')) {
                    include_once _PS_MODULE_DIR_ . 'multitrackingbo/classes/MTBOrderInvoice.php';

                    if (!count(MTBOrderInvoice::getByDeliveryDateInterval(
                        Tools::getValue('date_from'),
                        Tools::getValue('date_to')
                    ))) {
                        $this->errors[] = $this->trans('No delivery slip was found for this period.', array(), 'Admin.Orderscustomers.Notification');
                    }
                } else {
                    if (!count(OrderInvoice::getByDeliveryDateInterval(
                        Tools::getValue('date_from'),
                        Tools::getValue('date_to')
                    ))) {
                        $this->errors[] = $this->trans('No delivery slip was found for this period.', array(), 'Admin.Orderscustomers.Notification');
                    }
                }

                if (!count($this->errors)) {
                    Tools::redirectAdmin(
                        $this->context->link->getAdminLink(
                            'AdminPdf'
                        ).'&submitAction=generateDeliverySlipsPDF&date_from='.
                        urlencode(Tools::getValue('date_from')).'&date_to='.
                        urlencode(Tools::getValue('date_to'))
                    );
                }
            }
        } else {
            parent::postProcess();
        }
    }
}
