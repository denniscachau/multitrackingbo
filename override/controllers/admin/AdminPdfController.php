<?php
/**
 * 2007-2023 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2023 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

class AdminPdfController extends AdminPdfControllerCore
{
    public function processGenerateDeliverySlipsPDF()
    {
        if (Configuration::get('MTB_generateSlips')) {
            include_once _PS_MODULE_DIR_ . 'multitrackingbo/classes/MTBOrderInvoice.php';
            include_once _PS_MODULE_DIR_ . 'multitrackingbo/classes/HTMLTemplateMtbDeliverySlip.php';

            $order_invoice_collection = MTBOrderInvoice::getByDeliveryDateInterval(
                Tools::getValue('date_from'),
                Tools::getValue('date_to')
            );
            if (!count($order_invoice_collection)) {
                die($this->trans('No invoice was found.', array(), 'Admin.Orderscustomers.Notification'));
            }
            $this->generatePDF($order_invoice_collection, 'MtbDeliverySlip');
        } else {
            parent::processGenerateDeliverySlipsPDF();
        }
    }
}
