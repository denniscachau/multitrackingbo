<?php
/**
 * 2007-2023 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2023 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

class MTBOrderInvoice extends OrderInvoice
{
    public $id_order_carrier;

    public function getProductsDetail()
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
        SELECT od.*, p.*, ps.*, mtb.`quantity` AS "product_quantity"
        FROM `' . _DB_PREFIX_ . 'order_detail` od
        LEFT JOIN `' . _DB_PREFIX_ . 'order_carrier` oc
            ON od.`id_order` = oc.id_order
        INNER JOIN `' . _DB_PREFIX_ . 'multitrackingbo_products` mtb
            ON oc.`id_order_carrier` = mtb.`id_order_carrier`
                AND od.`product_id` = mtb.`id_product`
                AND od.`product_attribute_id` = mtb.`id_product_attribute`
                AND mtb.`id_order_carrier` = ' . (int)$this->id_order_carrier . '
        INNER JOIN `' . _DB_PREFIX_ . 'multitrackingbo_invoices` `mtb_invoices`
            ON mtb.id_order_carrier = `mtb_invoices`.`id_order_carrier`
                AND `mtb_invoices`.`id_invoice` = ' . (int)$this->id . '
        LEFT JOIN `' . _DB_PREFIX_ . 'product` p
        ON p.id_product = od.product_id
        LEFT JOIN `' . _DB_PREFIX_ . 'product_shop` ps ON (ps.id_product = p.id_product AND ps.id_shop = od.id_shop)
        WHERE od.`id_order` = ' . (int) $this->id_order . '
        ' . ($this->id && $this->number ? ' AND od.`id_order_invoice` = ' . (int) $this->id : '') . ' 
        ORDER BY od.`product_name`');
    }

    /**
     * @since 1.5.0.3
     *
     * @param $date_from
     * @param $date_to
     *
     * @return array collection of invoice
     */
    public static function getByDeliveryDateInterval($date_from, $date_to)
    {
        $order_invoice_list = Db::getInstance()->executeS('
            SELECT oi.*, mtb.`id_order_carrier`
            FROM `' . _DB_PREFIX_ . 'order_invoice` oi
            INNER JOIN `' . _DB_PREFIX_ . 'multitrackingbo_invoices` mtb
                ON oi.`id_order_invoice` = mtb.`id_invoice`
            LEFT JOIN `' . _DB_PREFIX_ . 'orders` o ON (o.`id_order` = oi.`id_order`)
            WHERE DATE_ADD(oi.delivery_date, INTERVAL -1 DAY) <= \'' . pSQL($date_to) . '\'
            AND oi.delivery_date >= \'' . pSQL($date_from) . '\'
            ' . Shop::addSqlRestriction(Shop::SHARE_ORDER, 'o') . '
            ORDER BY oi.delivery_date ASC
        ');

        $collection = array();
        $object = new MTBOrderInvoice();
        foreach ($order_invoice_list as $order_invoice) {
            $obj = clone $object;
            $obj->hydrate($order_invoice);
            $collection[] = $obj;
        }

        return $collection;
    }
}
