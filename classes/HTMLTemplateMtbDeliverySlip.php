<?php
/**
 * 2007-2023 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2023 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

include_once _PS_MODULE_DIR_ . 'multitrackingbo/classes/MTB.php';

class HTMLTemplateMtbDeliverySlip extends HTMLTemplateDeliverySlip
{
    private $mtb;

    public function __construct(OrderInvoice $order_invoice, $smarty, $bulk_mode = false)
    {
        parent::__construct($order_invoice, $smarty, $bulk_mode);

        $this->mtb = MTB::getTemplateInfo($order_invoice->id, $order_invoice->id_order_carrier);

        // Change the invoice date by the order_carrier one
        // $this->date = Tools::displayDate($this->mtb['date_add']);
        $this->title .=  "-" . (int)$this->mtb['number'];
        $this->order->id_carrier = (int)$this->mtb['id_carrier'];
    }

    /**
     * Returns the template filename.
     *
     * @return string filename
     */
    public function getFilename()
    {
        return Configuration::get(
            'PS_DELIVERY_PREFIX',
            Context::getContext()->language->id,
            null,
            $this->order->id_shop
        ) . sprintf('%06d', $this->order->delivery_number) . '-' . (int)$this->mtb['number'] . '.pdf';
    }
}
