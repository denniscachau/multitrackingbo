<?php
/**
 * 2007-2023 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2023 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class MTB
{
    /**
     * @param string[] $id_products
     */
    public static function getTotalWeight($id_products, $weight = false)
    {
        if ($weight !== false) {
            return (float)$weight;
        }

        $total_weight = 0;
        foreach ($id_products as $id_product) {
            $id_product = explode(';', $id_product);
            $product = new Product($id_product[0]);
            $combination = new Combination($id_product[1]);

            $combination_weight = Validate::isLoadedObject($combination) ? (float)$combination->weight : 0;
            $product_weight = Validate::isLoadedObject($product) ? (float)$product->weight : 0;

            $total_weight += $product_weight + $combination_weight;
        }

        return (float)$total_weight;
    }

    public static function getShipping($order)
    {
        $results = Db::getInstance()->executeS(
            'SELECT DISTINCT oc.`id_order_invoice`, oc.`weight`, oc.`shipping_cost_tax_excl`, 
                oc.`shipping_cost_tax_incl`, c.`url`, oc.`id_carrier`, c.`name` as `carrier_name`, 
                oc.`date_add`, "Delivery" as `type`, "true" as `can_edit`, oc.`tracking_number`, 
                oc.`id_order_carrier`, c.`name` as "state_name", 
                IFNULL(d.`id_invoice`, 0) AS "id_invoice"
            FROM `' . _DB_PREFIX_ . 'orders` o
            LEFT JOIN `' . _DB_PREFIX_ . 'order_carrier` oc
                ON (o.`id_order` = oc.`id_order`)
            LEFT JOIN `' . _DB_PREFIX_ . 'carrier` c
                ON (oc.`id_carrier` = c.`id_carrier`)
            LEFT JOIN `' . _DB_PREFIX_ . 'multitrackingbo_invoices` d
                ON (oc.`id_order_carrier` = d.`id_order_carrier`)
                    AND d.`active` = 1
            WHERE o.`id_order` = ' . (int) $order->id . '
            ORDER BY oc.`id_order_carrier`'
        );

        foreach ($results as &$row) {
            if (version_compare(_PS_VERSION_, '8.0.0', '<')) {
                $row['carrier_name'] = Cart::replaceZeroByShopName($row['carrier_name'], null);
            }
        }

        return $results;
    }

    public static function isProductUseCarrier($id_order_carrier, $id_product, $id_product_attribute)
    {
        return (int)Db::getInstance()->getValue(
            "SELECT `quantity`
            FROM `" . _DB_PREFIX_ . "multitrackingbo_products`
            WHERE `id_order_carrier` = '" . (int)$id_order_carrier . "' 
                AND `id_product` = '" . (int)$id_product . "'
                AND `id_product_attribute` = '" . (int)$id_product_attribute . "'"
        );
    }

    public static function hasNoDuplicate($id_order_carrier, $id_order, $order_products, $order_carrier_product)
    {
        $mtb_quantity = (int)Db::getInstance()->getValue(
            "SELECT SUM(`a`.`quantity`) AS 'quantity'
            FROM `" . _DB_PREFIX_ . "multitrackingbo_products` `a`
            LEFT JOIN `" . _DB_PREFIX_ . "order_carrier` `b`
                ON `a`.`id_order_carrier` = `b`.`id_order_carrier`
            WHERE `a`.`id_order_carrier` <> " . (int)$id_order_carrier . "
                AND `b`.`id_order` = " . (int)$id_order . "
                AND `a`.`id_product` = " . (int)$order_carrier_product['id_product'] . "
                AND `a`.`id_product_attribute` = " . (int)$order_carrier_product['id_product_attribute'] . "
            GROUP BY `a`.`id_product`, `a`.`id_product_attribute`"
        );

        $quantities = array();
        foreach ($order_products as $order_product) {
            $quantities[$order_product['product_id'] . ';' . $order_product['product_attribute_id']] =
                (int)$order_product['product_quantity'];
        }

        $id_product = (int)$order_carrier_product['id_product'];
        $id_product_attribute = (int)$order_carrier_product['id_product_attribute'];

        if ((int)($order_carrier_product['quantity'] + $mtb_quantity) > $quantities[
            $id_product . ';' . $id_product_attribute
            ]) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param Order $order
     * @param OrderCarrier $order_carrier
     *
     * @return bool
     * @throws PrestaShopDatabaseException
     */
    public static function refreshShippingCost($order, $order_carrier, $shipping_price = false)
    {
        if (empty($order->id)) {
            return false;
        }

        if (!Configuration::get('PS_ORDER_RECALCULATE_SHIPPING')) {
            return true;
        }

        if ($shipping_price !== false) {
            $carrier_tax = 0;
            // Select carrier tax
            if (!Tax::excludeTaxeOption()) {
                $address = Address::initialize((int) $order->id_address_delivery);

                if (!Configuration::get('PS_ATCP_SHIPWRAP')) {
                    $carrier = new Carrier($order_carrier->id_carrier);
                    if (Validate::isLoadedObject($carrier)) {
                        $carrier_tax = $carrier->getTaxesRate($address);
                    }
                }
            }

            // get new shipping cost
            // TODO check if we need to use the TAX EXCL or INCL
            if (false) {
                $base_total_shipping_tax_excl = $shipping_price;
                $base_total_shipping_tax_incl = $shipping_price * (1 + ($carrier_tax / 100));
            } else {
                $base_total_shipping_tax_excl = $shipping_price / (1 + ($carrier_tax / 100));
                $base_total_shipping_tax_incl = $shipping_price;
            }
        } else {
            $fake_cart = new Cart((int) $order->id_cart);
            $new_cart = $fake_cart->duplicate();
            $new_cart = $new_cart['cart'];

            // assign order id_address_delivery to cart
            $new_cart->id_address_delivery = (int) $order->id_address_delivery;

            $products = Db::getInstance()->executeS(
                "SELECT * 
            FROM `" . _DB_PREFIX_ . "multitrackingbo_products`
            WHERE `id_order_carrier` = " . (int)$order_carrier->id
            );

            //remove all products : cart (maybe change in the meantime)
            foreach ($new_cart->getProducts() as $product) {
                $new_cart->deleteProduct((int) $product['id_product'], (int) $product['id_product_attribute']);
            }

            // add real order products
            foreach ($products as $product) {
                $new_cart->updateQty(
                    $product['quantity'],
                    (int) $product['id_product'],
                    (int) $product['id_product_attribute'],
                    false,
                    'up',
                    0,
                    null,
                    true,
                    true
                ); // - skipAvailabilityCheckOutOfStock
            }

            $address = new Address((int)$order->id_address_delivery);

            if (Validate::isLoadedObject($address)) {
                $country = new Country($address->id_country);
            } else {
                $country = null;
            }
            $products = array_map(function ($product) use ($order) {
                return array_merge($product, [
                    'id_address_delivery' => $order->id_address_delivery
                ]);
            }, $new_cart->getProducts());

            // get new shipping cost
            $base_total_shipping_tax_excl = (float)$new_cart->getPackageShippingCost(
                (int)$order_carrier->id_carrier,
                false,
                $country,
                $products
            );
            $base_total_shipping_tax_incl = (float)$new_cart->getPackageShippingCost(
                (int)$order_carrier->id_carrier,
                true,
                $country,
                $products
            );

            // remove fake cart
            $new_cart->delete();
        }

        $order_carrier->shipping_cost_tax_excl = $base_total_shipping_tax_excl;
        $order_carrier->shipping_cost_tax_incl = $base_total_shipping_tax_incl;
        $order_carrier->update();

        $cost_total = Db::getInstance()->getRow(
            "SELECT SUM(`shipping_cost_tax_excl`) as 'shipping_cost_tax_excl', 
            SUM(`shipping_cost_tax_incl`) as 'shipping_cost_tax_incl'
            FROM `" . _DB_PREFIX_ . "order_carrier`
            WHERE `id_order` = " . (int) $order->id
        );

        $order->total_shipping_tax_excl = (float)$cost_total['shipping_cost_tax_excl'];
        $order->total_shipping_tax_incl = (float)$cost_total['shipping_cost_tax_incl'];
        $order->total_shipping = $cost_total['shipping_cost_tax_incl'];
        $order->total_paid_tax_excl =
            (float)($order->total_products + $order->total_wrapping_tax_excl +
            $cost_total['shipping_cost_tax_excl'] - $order->total_discounts_tax_excl);
        $order->total_paid_tax_incl =
            (float)($order->total_products_wt + $order->total_wrapping_tax_excl +
            $cost_total['shipping_cost_tax_incl'] - $order->total_discounts_tax_incl);
        $order->total_paid = (float)$order->total_paid_tax_incl;
        $order->update();

        return true;
    }

    /**
     * @param Order $order
     * @param OrderCarrier $order_carrier
     * @return bool
     */
    public static function deleteShippingCost($order)
    {
        if (empty($order->id)) {
            return false;
        }

        $cost_total = Db::getInstance()->getRow(
            "SELECT SUM(`shipping_cost_tax_excl`) as 'shipping_cost_tax_excl', 
            SUM(`shipping_cost_tax_incl`) as 'shipping_cost_tax_incl'
            FROM `" . _DB_PREFIX_ . "order_carrier`
            WHERE `id_order` = " . (int) $order->id
        );

        $order->total_shipping_tax_excl = (float)$cost_total['shipping_cost_tax_excl'];
        $order->total_shipping_tax_incl = (float)$cost_total['shipping_cost_tax_incl'];
        $order->total_shipping = (float)$cost_total['shipping_cost_tax_incl'];
        $order->total_paid_tax_excl = (float)($order->total_products + $cost_total['shipping_cost_tax_excl']);
        $order->total_paid_tax_incl = (float)($order->total_products_wt + $cost_total['shipping_cost_tax_incl']);
        $order->total_paid = (float)$order->total_paid_tax_incl;

        $order->update();
        return true;
    }

    public static function generateDeliverySlipDb($id_order_carrier, $id_invoice)
    {
        $id_invoice_exists = (int)Db::getInstance()->getValue(
            "SELECT `id_invoice`
            FROM `" . _DB_PREFIX_ . "multitrackingbo_invoices`
            WHERE `id_order_carrier` = " . (int)$id_order_carrier . " AND `active` = 1"
        );

        if ($id_invoice_exists > 0 && $id_invoice_exists == $id_invoice) {
            return true;
        } elseif ($id_invoice_exists > 0 && $id_invoice_exists != $id_invoice) {
            // set to inactive the row with id_order_carrier and id_invoice_exists
            Db::getInstance()->execute(
                "UPDATE `" . _DB_PREFIX_ . "multitrackingbo_invoices`
                SET `active` = 0
                WHERE `id_order_carrier` = " . (int)$id_order_carrier . " 
                    AND `id_invoice` = " . (int)$id_invoice_exists
            );
        }

        $number = (int)Db::getInstance()->getValue(
            "SELECT `number`
            FROM `" . _DB_PREFIX_ . "multitrackingbo_invoices`
            WHERE `id_order_carrier` = " . (int)$id_order_carrier . " 
                AND `id_invoice` = " . (int)$id_invoice
        );

        if ($number == 0) {
            $number = (int)Db::getInstance()->getValue(
                "SELECT MAX(`number`)
                FROM `" . _DB_PREFIX_ . "multitrackingbo_invoices`
                WHERE `id_invoice` = " . (int)$id_invoice
            )+1;
        }

        Db::getInstance()->execute(
            "INSERT INTO `" . _DB_PREFIX_ . "multitrackingbo_invoices` (`id_order_carrier`, `id_invoice`, `number`)
            VALUES (" . (int)$id_order_carrier . ", " . (int)$id_invoice . ", " . (int)$number . ")"
        );

        return true;
    }

    public static function getTemplateInfo($id_invoice, $id_order_carrier)
    {
        return Db::getInstance()->getRow(
            "SELECT mtb.`number`, oc.`date_add`, oc.`id_carrier`
            FROM `" . _DB_PREFIX_ . "multitrackingbo_invoices` mtb
            LEFT JOIN `" . _DB_PREFIX_ . "order_carrier` oc
                ON mtb.`id_order_carrier` = oc.`id_order_carrier`
            WHERE mtb.id_invoice = " . (int)$id_invoice . " AND mtb.`id_order_carrier` = " . (int)$id_order_carrier
        );
    }
}
